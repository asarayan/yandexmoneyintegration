﻿using System;
using System.IO;
using System.Net;
using YandexMoneyListener.Models;
using YandexMoneyListener.Services;

namespace YandexMoneyListener
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            var moneyService = new YandexMoneyService();
            SynonymCardModel synonymCardModel = moneyService.GetSynonymCard("4444444444444448");

            if (synonymCardModel.reason != "success")
            {
                Console.WriteLine(synonymCardModel.reason);
                return;
            }

            var depositionModel = new DepositionModel
            {
                AgentId = "201131",
                DstAccount = "257003392579",
                ClientOrderId = rnd.Next(10, 10000000).ToString(),
                RequestDt = DateTime.Now.ToString("o"),
                Amount = "75.00",
                Currency = "10643",
                Contract = "Потому что заслужил",
                PaymentParamsModel = new PaymentParamsModel
                {
                    CardSynonim = synonymCardModel,
                    FirstName = "Аркадий",
                    MiddleName = "Михайлович",
                    LastName = "Сараян",
                    DocNumber = "4002109067",
                    PostCode = "194044",
                    Country = "643",
                    City = "Новосибирск",
                    Address = "Титова 29/1",
                    BirthDate = "11.06.1988",
                    BirthPlace = "Новосибирск",
                    DocIssueYear = "1999",
                    DocIssueMonth = "07",
                    DocIssueDay = "30",
                    DocIssuedBy = "ТП №20 по СПб и ЛО",
                    OfferAccepted = "1",
                    SmsPhoneNumber = "79612155115"
                }
            };
            Console.WriteLine(depositionModel.ConvertToXml());
            var result = moneyService.TestDepositionToBankCard(depositionModel);
            //var result = moneyService.MakeDepositionToBankCard(makeDepositionModel);
            Console.WriteLine(result.Status + result.Error);
            Console.ReadKey();
        }

       
    }
}
