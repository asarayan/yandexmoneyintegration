﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using YandexMoneyListener.Classes;
using YandexMoneyListener.Models;
using Org.BouncyCastle.Asn1.Cms;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Cms;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Pkcs;
using X509Certificate = Org.BouncyCastle.X509.X509Certificate;

namespace YandexMoneyListener.Services
{
    public class YandexMoneyService : IYandexMoneyService
    {
        readonly Httpgp3Session _session;

        public YandexMoneyService()
        {
            _session = new Httpgp3Session(
                "201131.p12", "650895");
        }

        public YandexResponse TestDepositionToBankCard(DepositionModel depositionModel)
        {
            var url = "https://bo-demo02.yamoney.ru:9094/webservice/deposition/api/testDeposition";
            depositionModel.Type = "test";
            return PostDeposition(depositionModel, url);
        }

        public YandexResponse MakeDepositionToBankCard(DepositionModel depositionModel)
        {
            var url = "https://bo-demo02.yamoney.ru:9094/webservice/deposition/api/makeDeposition";
            depositionModel.Type = "make";
            return PostDeposition(depositionModel, url);
        }

        public YandexResponse PostDeposition(DepositionModel depositionModel, string url)
        {
            var clientCertificate = new X509Certificate2("201131.p12", "650895");
            var clientCertFileName = "201131.p12";
            var serverX509Certificate2 = new X509Certificate2("deposit.cer");
            var serverCertificate = Org.BouncyCastle.Security.DotNetUtilities.FromX509Certificate(serverX509Certificate2);
            var myWebClient = new CertificateWebClient(clientCertificate);
            var request = (HttpWebRequest)WebRequest.Create(url);

            request.ClientCertificates.Add(clientCertificate);
            myWebClient.UploadFile(url, clientCertFileName);
            request.ProtocolVersion = HttpVersion.Version11;
            request.Method = "POST";
            request.ContentType = "application/pkcs7-mime";
            string testRequest = _session.Sign(Encoding.UTF8.GetBytes(depositionModel.ConvertToXml()), _session._clientKeys);
            using (var stream = request.GetRequestStream())
            {
                byte[] testRequestBytes = Encoding.UTF8.GetBytes(testRequest);
                stream.Write(testRequestBytes, 0, testRequestBytes.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();
            var result = new StreamReader(response.GetResponseStream()).ReadToEnd();

            //проверка ответа
            YandexResponse testResponseDocument = _session.Verify(result, serverCertificate).XmlToResponse();
            return testResponseDocument;
        }

        public YandexResponse MakeDepositionToYandexPurse()
        {
            throw new NotImplementedException();
        }

        public YandexResponse MakeDepositionToBankAccount()
        {
            throw new NotImplementedException();
        }

        public YandexResponse MakeDepositionToMobileAccount()
        {
            throw new NotImplementedException();
        }

        public SynonymCardModel GetSynonymCard(string cardNumber)
        {
            var url =
                $"https://demo-scrat.yamoney.ru/gates/card/storeCard/?skr_destinationCardNumber={cardNumber}&skr_responseFormat=json";

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            var response = (HttpWebResponse)request.GetResponse();


            if (response.StatusCode != HttpStatusCode.OK) return null;

            var jsonString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            var json =  JObject.Parse(jsonString).SelectToken("storeCard");

            return JsonConvert.DeserializeObject<SynonymCardModel>(json.ToString());
        }
    }
}
