﻿using YandexMoneyListener.Classes;
using YandexMoneyListener.Models;

namespace YandexMoneyListener.Services
{
    public interface IYandexMoneyService
    {
        YandexResponse TestDepositionToBankCard(DepositionModel depositionModel);
        YandexResponse MakeDepositionToBankCard(DepositionModel depositionModel);
        

        SynonymCardModel GetSynonymCard(string cardNumber);
    }
}
