﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YandexMoneyListener.Classes
{
    public class YandexResponse
    {
        string Code { get; set; }
        string Messgae { get; set; }
        public string Status { get; set; }
        public string ClientOrderId { get; set; }
        public string ProcessedDT { get; set; }
        public string Error { get; set; }
    }
}
