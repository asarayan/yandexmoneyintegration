﻿using System;
using System.Xml;

namespace YandexMoneyListener.Classes
{
    /// <summary>
    /// Пример клиента протокола
    /// "Перевод средств на счета в системе «Яндекс.Деньги», HTTP-транспорт (HTTPGP-3.0)"
    /// на .NET 3.5
    /// Пример содержит процедуры создания и проверки сообщений протокола для сервера и клиента
    /// Для запуска примера потребуется:
    /// 1. .NET 3.5
    /// 2. библиотека BouncyCastle http://www.bouncycastle.org/
    /// </summary>
    internal class Httpgp3Session : CommonProtocolUtils
    {
        /**
     * Параметры приказа на зачисление
     */

        // идентификатор агента
        private readonly int _agentId;
        private readonly Decimal _amount;
        private readonly Decimal _balance;
        // идентификатор операции клиента
        private readonly long _clientOrderId;
        private readonly string _contract;
        private readonly int _currency;
        // время формирования документа (запроса)
        private readonly DateTime _requestDt;
        // номер счета получателя
        private readonly string _dstAccount;
        // сумма зачисления
        // код ошибки
        private readonly int _error;
        // время обработки приказа на стороне сервера
        private readonly DateTime _processedDt;
        private readonly int _status;
        // остаток на счете агента

        internal Httpgp3Session(String clientKeystorePath, String clientKeystorePass)
            : base(clientKeystorePath, clientKeystorePass)
        {
            // текущее время в зоне UTC
            DateTime now = DateTime.Now.ToUniversalTime();

           /**
            * Инициализация параметров случайнвми значениями
            */

            // инициализация параметров отправляемого приказа
            _agentId = (int) (now.Ticks & 0xFFFF);
            _clientOrderId = now.Ticks;
            _requestDt = now;
            _dstAccount = "41001101140";
            var rnd = new Random();
            _amount = Decimal.Round(new Decimal(rnd.Next(100)), 2); // случайное число до 100 руб.
            _currency = 643;
            _contract = "Тестовый платеж, транзакция номер: " + _clientOrderId;

            // инициализация параметров ответа сервера
            _status = rnd.Next(1) == 1 ? 0 : 3;
            _error = _status == 0 ? 0 : rnd.Next(50);
            _processedDt = new DateTime(now.Ticks + rnd.Next(100));
            _balance = Decimal.Round(new Decimal(rnd.Next(1000000)), 2);
        }

       

        /// <summary>
        /// Создание XML документа запроса "Проверка возможности зачисления"
        /// </summary>
        /// <returns>XML документ в сериализованном виде</returns>
        public byte[] CreateTestDepositionRequestDocument()
        {
            var doc = CreateXMLDocument();
            XmlElement root = doc.CreateElement("", "testDepositionRequest", "");
            doc.AppendChild(root);
            WriteCommonRequestAttributes(root);
            return Serialize(doc);
        }

        /// <summary>
        /// Создание XML документа ответа на запрос "Проверка возможности зачисления"
        /// </summary>
        /// <returns>XML документ в сериализованном виде</returns>
        public byte[] CreateTestDepositionResponseDocument()
        {
            var doc = CreateXMLDocument();
            XmlElement root = doc.CreateElement("", "testDepositionResponse", "");
            WriteCommonResponseAttributes(root);
            doc.AppendChild(root);
            return Serialize(doc);
        }

        /// <summary>
        /// Создание XML документа запроса "Зачисление на счет"
        /// </summary>
        /// <returns>XML документ в сериализованном виде</returns>
        public byte[] CreateMakeDepositionRequestDocument()
        {
            var doc = CreateXMLDocument();
            XmlElement root = doc.CreateElement("makeDepositionRequest");
            doc.AppendChild(root);
            WriteCommonRequestAttributes(root);
            return Serialize(doc);
        }

        /// <summary>
        /// Создание XML документа запроса "Зачисление на счет"
        /// </summary>
        /// <returns>XML документ в сериализованном виде</returns>
        public byte[] CreateMakeDepositionResponseDocument()
        {
            var doc = CreateXMLDocument();
            XmlElement root = doc.CreateElement("makeDepositionResponse");
            doc.AppendChild(root);
            WriteCommonResponseAttributes(root);
            if (_status == 0)
            {
                root.SetAttribute("balance", XmlConvert.ToString(_balance));
            }
            return Serialize(doc);
        }

        private void WriteCommonRequestAttributes(XmlElement root)
        {
            root.SetAttribute("agentId", XmlConvert.ToString(_agentId));
            root.SetAttribute("clientOrderId", XmlConvert.ToString(_clientOrderId));
            root.SetAttribute("requestDT", XmlConvert.ToString(_requestDt, XmlDateTimeSerializationMode.Utc));
            root.SetAttribute("dstAccount", _dstAccount);
            root.SetAttribute("amount", XmlConvert.ToString(_amount));
            root.SetAttribute("currency", XmlConvert.ToString(_currency));
            root.SetAttribute("contract", _contract ?? "");
        }

        private void WriteCommonResponseAttributes(XmlElement root)
        {
            root.SetAttribute("clientOrderId", XmlConvert.ToString(_clientOrderId));
            root.SetAttribute("status", XmlConvert.ToString(_status));
            root.SetAttribute("error", XmlConvert.ToString(_error));
            root.SetAttribute("processedDT", XmlConvert.ToString(_processedDt, XmlDateTimeSerializationMode.Utc));
        }

        /// <summary>
        /// Разбор XML документа запроса "Проверка возможности зачисления"
        /// </summary>
        /// <param name="request">XML документ в сериализованном виде</param>
        public void VerifyTestDepositionRequestDocument(byte[] request)
        {
            XmlElement root = Deserialize(request);
            if (!"testDepositionRequest".Equals(root.Name))
            {
                throw new ArgumentException("Unknown root element");
            }
            VerifyCommonRequestAttributes(root);
        }

        /// <summary>
        /// Разбор XML документа ответа на запрос "Проверка возможности зачисления"
        /// </summary>
        /// <param name="request">XML документ в сериализованном виде</param>
        public void VerifyTestDepositionResponseDocument(byte[] request)
        {
            XmlElement root = Deserialize(request);
            if (!"testDepositionResponse".Equals(root.Name))
            {
                throw new ArgumentException("Unknown root element");
            }
            VerifyCommonResponseAttributes(root);
        }

        /// <summary>
        /// Разбор XML документа запроса "Зачисление на счет"
        /// </summary>
        /// <param name="request">request XML документ в сериализованном виде</param>
        public void VerifyMakeDepositionRequestDocument(byte[] request)
        {
            XmlElement root = Deserialize(request);
            if (!"makeDepositionRequest".Equals(root.Name))
            {
                throw new ArgumentException("Unknown root element");
            }
            VerifyCommonRequestAttributes(root);
        }

        /// <summary>
        /// Разбор XML документа ответа на запрос "Зачисление на счет"
        /// </summary>
        /// <param name="request">XML документ в сериализованном виде</param>
        public void VerifyMakeDepositionResponseDocument(byte[] request)
        {
            XmlElement root = Deserialize(request);
            if (!"makeDepositionResponse".Equals(root.Name))
            {
                throw new ArgumentException("Unknown root element");
            }
            int status = VerifyCommonResponseAttributes(root);
            if (status == 0)
            {
                string balance = root.GetAttribute("balance");
                if (balance.Length == 0)
                {
                    throw new ArgumentException("balance missing");
                }
                if (_balance.CompareTo(XmlConvert.ToDecimal(balance)) != 0)
                {
                    throw new ArgumentException("balance wrong");
                }
            }
        }

        private void VerifyCommonRequestAttributes(XmlElement root)
        {
            string agentId = root.GetAttribute("agentId");
            if (agentId.Length == 0)
            {
                throw new ArgumentException("agentId missing");
            }
            if (XmlConvert.ToInt32(agentId) != _agentId)
            {
                throw new ArgumentException("agentId wrong");
            }

            string clientOrderId = root.GetAttribute("clientOrderId");
            if (clientOrderId.Length == 0)
            {
                throw new ArgumentException("clientOrderId missing");
            }
            if (XmlConvert.ToInt64(clientOrderId) != _clientOrderId)
            {
                throw new ArgumentException("clientOrderId wrong");
            }

            string requestDt = root.GetAttribute("requestDT");
            if (requestDt.Length == 0)
            {
                throw new ArgumentException("requestDT missing");
            }
            if (XmlConvert.ToDateTime(requestDt, XmlDateTimeSerializationMode.Utc).CompareTo(_requestDt) != 0)
            {
                throw new ArgumentException("requestDT wrong");
            }

            string dstAccount = root.GetAttribute("dstAccount");
            if (dstAccount.Length == 0)
            {
                throw new ArgumentException("dstAccount missing");
            }
            if (!_dstAccount.Equals(dstAccount))
            {
                throw new ArgumentException("dstAccount wrong");
            }

            string amount = root.GetAttribute("amount");
            if (amount.Length == 0)
            {
                throw new ArgumentException("amount missing");
            }
            if (_amount.CompareTo(XmlConvert.ToDecimal(amount)) != 0)
            {
                throw new ArgumentException("amount wrong");
            }

            string currency = root.GetAttribute("currency");
            if (currency.Length == 0)
            {
                throw new ArgumentException("currency missing");
            }
            if (XmlConvert.ToInt32(currency) != _currency)
            {
                throw new ArgumentException("currency wrong");
            }

            string contract = root.GetAttribute("contract");
            if (contract.Length == 0)
            {
                throw new ArgumentException("contract missing");
            }
            if (!_contract.Equals(contract))
            {
                throw new ArgumentException("contract wrong");
            }
        }

        private int VerifyCommonResponseAttributes(XmlElement root)
        {
            string clientOrderId = root.GetAttribute("clientOrderId");
            if (clientOrderId.Length == 0)
            {
                throw new ArgumentException("clientOrderId missing");
            }
            if (XmlConvert.ToInt64(clientOrderId) != _clientOrderId)
            {
                throw new ArgumentException("clientOrderId wrong");
            }

            string status = root.GetAttribute("status");
            if (status.Length == 0)
            {
                throw new ArgumentException("status missing");
            }
            int nStatus = XmlConvert.ToInt32(status);
            if (nStatus != _status)
            {
                throw new ArgumentException("status wrong");
            }

            string error = root.GetAttribute("error");
            if (error.Length == 0)
            {
                throw new ArgumentException("error missing");
            }
            if (XmlConvert.ToInt32(error) != _error)
            {
                throw new ArgumentException("error wrong");
            }

            string processedDt = root.GetAttribute("processedDT");
            if (processedDt.Length == 0)
            {
                throw new ArgumentException("processedDT missing");
            }
            if (XmlConvert.ToDateTime(processedDt, XmlDateTimeSerializationMode.Utc).CompareTo(_processedDt) != 0)
            {
                throw new ArgumentException("processedDT wrong");
            }

            return nStatus;
        }
    }
}