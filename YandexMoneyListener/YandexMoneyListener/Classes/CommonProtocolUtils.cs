﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;
using Org.BouncyCastle.Asn1.Cms;
using Org.BouncyCastle.Cms;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.X509;

namespace YandexMoneyListener.Classes
{
    internal class CommonProtocolUtils
    {
        // ключевая пара клиента
        private const int TcpTimeout = 60000;
        private readonly NumberFormatInfo _amountFormat;
        internal readonly KeyPair _clientKeys;
        // ключевая пара сервера
        internal readonly KeyPair _serverKeys;

        /**
          * Настройки теста
          */
        // TCP timeout, миллисекунд

        // определение временной зоны (UTC)
        private readonly TimeZoneInfo _timeZone;
        // определение формата суммы (0.00)

        internal CommonProtocolUtils(String clientKeystorePath, String clientKeystorePass)
        {
            // загрузка пар ключей и сертификатов
            _clientKeys = LoadPkcs12(clientKeystorePath, clientKeystorePass);

            // инициализация форматов
            _timeZone = TimeZoneInfo.Utc;
            _amountFormat = new NumberFormatInfo {CurrencyDecimalSeparator = ".", NumberDecimalDigits = 2};
        }

        /// <summary>
        /// Загрузка ключевой пары из файла формата PKCS#12 (OpenSSL)
        /// </summary>
        /// <param name="filename">путь к файлу</param>
        /// <param name="password">пароль к файлу</param>
        /// <returns></returns>
        private KeyPair LoadPkcs12(String filename, String password)
        {
            var fin = new FileStream(filename, FileMode.Open);
            try
            {
                var store = new Pkcs12Store(fin, password.ToCharArray());
                IEnumerator aliases = store.Aliases.GetEnumerator();
                while (aliases.MoveNext())
                {
                    var alias = (string) aliases.Current;
                    AsymmetricKeyEntry privKey = store.GetKey(alias);
                    if (privKey != null)
                    {
                        return new KeyPair(privKey.Key, store.GetCertificate(alias).Certificate);
                    }
                }
                throw new ArgumentException("No private keys found");
            }
            finally
            {
                fin.Close();
            }
        }

        /// <summary>
        /// Создание пустого XML документа
        /// </summary>
        /// <returns></returns>
        internal XmlDocument CreateXMLDocument()
        {
            var doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(dec);
            return doc;
        }

        /// <summary>
        /// Сериализация XML документа
        /// </summary>
        /// <param name="doc">XML документ</param>
        /// <returns>сериализованные данные</returns>
        internal byte[] Serialize(XmlDocument doc)
        {
            var ms = new MemoryStream();
            XmlWriter w = new XmlTextWriter(ms, Encoding.UTF8);
            doc.WriteTo(w);
            w.Close();
            return ms.ToArray();
        }

        /// <summary>
        /// Десериализация XML документа
        /// </summary>
        /// <param name="data">сериализованные данные</param>
        /// <returns>корневой элемент разобранного документа</returns>
        internal XmlElement Deserialize(byte[] data)
        {
            var doc = new XmlDocument();
            doc.Load(new XmlTextReader(new MemoryStream(data)));
            return doc.DocumentElement;
        }

        /// <summary>
        /// Изготовление PKCS#7 пакета в PEM формате (OpenSSL), содержащего:
        /// 1. пользовательские данные 
        /// 2. подпись, изготовленную ключевой парой пользователя
        /// </summary>
        /// <param name="source">пользовательские данные</param>
        /// <param name="keyPair">ключевая пара для изготовления подписи</param>
        /// <returns>PKCS#7 пакет в сериализованном виде</returns>
        internal string Sign(byte[] source, KeyPair keyPair)
        {
            var gen = new CmsSignedDataGenerator();
            gen.AddSigner(keyPair.PrivateKey, keyPair.Certificate, CmsSignedGenerator.DigestSha1);
            CmsSignedData s = gen.Generate(new CmsProcessableByteArray(source), true);
            var sw = new StringWriter();
            var writer = new PemWriter(sw);
            writer.WriteObject(s.ContentInfo);
            writer.Writer.Close();
            return sw.ToString();
        }

        /// <summary>
        /// Распаковка PKCS#7 пакета в PEM формате (OpenSSL) и проверка подписи согласно требуемому сертификату
        /// </summary>
        /// <param name="data">PKCS#7 пакет в сериализованном виде</param>
        /// <param name="certificate">X509 сертификат, по которому сверяется подпись</param>
        /// <returns>пользовательские данные, извлеченные из пакета</returns>
        internal byte[] Verify(string data, X509Certificate certificate)
        {
            var reader = new PemReader(new StringReader(data));
            var signedData = new CmsSignedData((ContentInfo) reader.ReadObject());
            return (byte[])signedData.SignedContent.GetContent();
        }
    }

    internal class KeyPair
    {
        /// <summary>
        /// Сертификат (публичная часть ключевой пары)
        /// </summary>
        private readonly X509Certificate _certificate;

        /// <summary>
        /// Секретная часть ключевой пары
        /// </summary>
        private readonly AsymmetricKeyParameter _privateKey;

        public KeyPair(AsymmetricKeyParameter privateKey, X509Certificate certificate)
        {
            _privateKey = privateKey;
            _certificate = certificate;
        }

        public AsymmetricKeyParameter PrivateKey
        {
            get { return _privateKey; }
        }

        public X509Certificate Certificate
        {
            get { return _certificate; }
        }
    }
}