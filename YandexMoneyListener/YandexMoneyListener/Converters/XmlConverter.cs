﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using YandexMoneyListener;
using YandexMoneyListener.Classes;
using YandexMoneyListener.Models;

namespace YandexMoneyListener
{
    public static class XmlConverter
   {
        public static string ConvertToXml(this DepositionModel depositionModel)
       {
            var xml = new StringBuilder();
            xml.Append($"<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            xml.Append($"<{depositionModel.Type}DepositionRequest " +
                       $"agentId=\"{depositionModel.AgentId}\" " +
                       $"clientOrderId=\"{depositionModel.ClientOrderId}\" " +
                       $"requestDT=\"{depositionModel.RequestDt}\" " +
                       $"dstAccount=\"{depositionModel.DstAccount}\" " +
                       $"amount=\"{depositionModel.Amount}\" " +
                       $"currency=\"{depositionModel.Currency}\" " +
                       $"contract=\"{depositionModel.Contract}\"> ");

            xml.Append("<paymentParams>");
            xml.Append($"<skr_destinationCardSynonim>{depositionModel.PaymentParamsModel.CardSynonim.skr_destinationCardSynonim}</skr_destinationCardSynonim>");
            xml.Append($"<pdr_firstName>{depositionModel.PaymentParamsModel.FirstName}</pdr_firstName>" +
                       $"<pdr_middleName>{depositionModel.PaymentParamsModel.MiddleName}</pdr_middleName>" +
                       $"<pdr_lastName>{depositionModel.PaymentParamsModel.LastName}</pdr_lastName>" +
                       $"<pdr_docNumber>{depositionModel.PaymentParamsModel.DocNumber}</pdr_docNumber>" +
                       $"<pdr_postcode>{depositionModel.PaymentParamsModel.PostCode}</pdr_postcode>" +
                       $"<pdr_country>{depositionModel.PaymentParamsModel.Country}</pdr_country>" +
                       $"<pdr_city>{depositionModel.PaymentParamsModel.City}</pdr_city>" +
                       $"<pdr_address>{depositionModel.PaymentParamsModel.Address}</pdr_address>" +
                       $"<pdr_birthDate>{depositionModel.PaymentParamsModel.BirthDate}</pdr_birthDate>" +
                       $"<pdr_birthPlace>{depositionModel.PaymentParamsModel.BirthPlace}</pdr_birthPlace>" +
                       $"<pdr_docIssueYear>{depositionModel.PaymentParamsModel.DocIssueYear}</pdr_docIssueYear>" +
                       $"<pdr_docIssueMonth>{depositionModel.PaymentParamsModel.DocIssueMonth}</pdr_docIssueMonth>" +
                       $"<pdr_docIssueDay>{depositionModel.PaymentParamsModel.DocIssueDay}</pdr_docIssueDay>" +
                       $"<pdr_docIssuedBy>{depositionModel.PaymentParamsModel.DocIssuedBy}</pdr_docIssuedBy>" +
                       $"<pof_offerAccepted>{depositionModel.PaymentParamsModel.OfferAccepted}</pof_offerAccepted>" +
                       $"<smsPhoneNumber>{depositionModel.PaymentParamsModel.SmsPhoneNumber}</smsPhoneNumber>");
            xml.Append($"</paymentParams></{depositionModel.Type}DepositionRequest>");

            return xml.ToString();
        }

        public static YandexResponse XmlToResponse(this byte[] source)
        {
            var str = Encoding.UTF8.GetString(source);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(str);
            return new YandexResponse()
            {
                Status = doc.DocumentElement?.Attributes["status"]?.InnerText,
                ClientOrderId = doc.DocumentElement?.Attributes["clientOrderId"]?.InnerText,
                ProcessedDT = doc.DocumentElement?.Attributes["processedDT"]?.InnerText,
                Error = doc.DocumentElement?.Attributes["error"]?.InnerText
            };
        }
    }
}
